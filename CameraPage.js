import React from 'react';
import { Text, View, TouchableOpacity, StyleSheet, Platform, Image, Dimensions, ScrollView, TextInput } from 'react-native';
import * as Permissions from 'expo-permissions';
import * as Location from 'expo-location';
import { Camera } from 'expo-camera';
import { Ionicons } from '@expo/vector-icons';
import Constants from 'expo-constants';
import * as MediaLibrary from 'expo-media-library';
import { BackHandler } from 'react-native';
export default class CameraPage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            hasCameraPermission: null,
            type: Camera.Constants.Type.back,
            pictureSize: undefined,
            pictureSizes: [],
            pictureSizeId: 0,
            zoom: 0,
            preview: false,
            clickedImage: {},
            dataToSend: {
                name: null,
                phoneNumber: null,
                message: null,
                location: null,
                file: null
            }
        }
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }


    componentDidMount() {
        this._requestCameraPermission();
    }

    componentWillUnmount() {
        console.log('unmount', this.camera);
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);

    }
    _requestCameraPermission = async () => {
        const { status } = await Permissions.askAsync(Permissions.CAMERA);
        this.setState({
            hasCameraPermission: status === 'granted',
        });
    };


    handleBackButtonClick = () => {
        console.log(this.props.navigation);
        this.props.navigation.pop();
        return true;
    }

    takePicture = () => {
        if (this.camera) {
            this.camera.takePictureAsync({ onPictureSaved: this.onPictureSaved });
        }
    };

    onPictureSaved = async (photo) => {
        this.setState({
            preview: true,
            clickedImage: photo,
            dataToSend: {
                ...this.state.dataToSend,
                file: photo
            }
        })
        // const asset = await MediaLibrary.createAssetAsync(photo.uri);
        // this.props.pictureTaken(photo.uri);
    }

    collectPictureSizes = async () => {

        if (this.camera) {
            const pictureSizes = await this.camera.getAvailablePictureSizesAsync('4:3');
            let pictureSizeId = 0;
            if (Platform.OS === 'ios') {
                pictureSizeId = pictureSizes.indexOf('High');
            } else {
                // returned array is sorted in ascending order - default size is the largest one
                pictureSizeId = pictureSizes.length - 1;
            }
            this.setState({ pictureSizes, pictureSizeId, pictureSize: pictureSizes[pictureSizeId] });
        }
    };

    zoomOut = () => this.setState({ zoom: this.state.zoom - 0.1 < 0 ? 0 : this.state.zoom - 0.1 });
    zoomIn = () => this.setState({ zoom: this.state.zoom + 0.1 > 1 ? 1 : this.state.zoom + 0.1 });

    zoomButtons = () => {
        return (
            <View style={styles.zoomContainer}>
                <Ionicons name="ios-add" size={40} color='white' onPress={this.zoomOut} style={{ padding: 6 }} />
                <Ionicons name="ios-add" size={40} color='white' onPress={this.zoomIn} style={{ padding: 6 }} />
            </View>
        );
    }

    previousPictureSize = () => this.changePictureSize(1);
    nextPictureSize = () => this.changePictureSize(-1);

    changePictureSize = direction => {
        let newId = this.state.pictureSizeId + direction;
        const length = this.state.pictureSizes.length;
        if (newId >= length) {
            newId = 0;
        } else if (newId < 0) {
            newId = length - 1;
        }
        this.setState({ pictureSize: this.state.pictureSizes[newId], pictureSizeId: newId });
    };

    pictureSizePicker() {
        return (
            <View style={styles.pictureSizeChooser}>
                <Ionicons name="md-return-left" size={40} color='white' onPress={this.previousPictureSize} style={{ padding: 6 }} />
                <View style={styles.pictureSizeLabel}>
                    <Text style={styles.textStyle}>{this.state.pictureSize}</Text>
                </View>
                <Ionicons name="md-return-right" size={40} color='white' onPress={this.nextPictureSize} style={{ padding: 6 }} />
            </View>);
    }

    cameraModeButton() {
        return (<Ionicons name="ios-reverse-camera" type='ionicon' size={50} color='white' onPress={() => {
            this.setState({
                type: this.state.type === Camera.Constants.Type.back
                    ? Camera.Constants.Type.front
                    : Camera.Constants.Type.back,
            });
        }} style={{ flex: 1, justifyContent: 'center' }} />);
    }



    render() {
        return (
            <Camera
                ref={ref => { this.camera = ref }}
                style={styles.camera}
                onCameraReady={this.collectPictureSizes}
                type={this.state.type}
                flashMode={Camera.Constants.FlashMode.auto}
                autoFocus={Camera.Constants.AutoFocus.on}
                zoom={this.state.zoom}
                whiteBalance={Camera.Constants.WhiteBalance.auto}
                pictureSize={this.state.pictureSize}>
                <View style={styles.topBar}>
                    {this.pictureSizePicker()}
                    <Ionicons name='md-close' color='white' size={30} onPress={() => Actions.pop()} style={{ position: 'absolute', top: 20, left: 10 }} />
                </View>
                <View style={styles.bottomBar}>
                    {this.zoomButtons()}
                    <Ionicons name='md-radio-button-off' color='white' size={70} onPress={this.takePicture} style={{ flex: 1, padding: 6 }} />
                    {/* {this.cameraModeButton()} */}
                </View>
            </Camera>
        );
    }
}