import React from 'react';
import { Text, View, TouchableOpacity, StyleSheet, Platform, Image, Dimensions, ScrollView, TextInput } from 'react-native';
import * as Permissions from 'expo-permissions';
import * as Location from 'expo-location';
import { Camera } from 'expo-camera';
import { Ionicons } from '@expo/vector-icons';
import Constants from 'expo-constants';
import * as MediaLibrary from 'expo-media-library';
import { BackHandler } from 'react-native';

const { height, width } = Dimensions.get('window');

const styles = StyleSheet.create({
  camera: {
    flex: 1,
    backgroundColor: '#000',
  },
  zoomContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row'
  },
  pictureSizeChooser: {
    justifyContent: 'center',
    flexDirection: 'row'
  },
  pictureSizeLabel: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  textStyle: {
    color: 'white',
    fontSize: 20
  },
  topBar: {
    paddingTop: Constants.statusBarHeight / 2,
    backgroundColor: 'transparent',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  bottomBar: {
    position: 'absolute',
    bottom: 0,
    flexDirection: 'row',
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  input: {
    margin: 15,
    height: 40,
    borderColor: '#7a42f4',
    borderWidth: 1
  },
  submitButton: {
    backgroundColor: '#7a42f4',
    padding: 10,
    margin: 15,
    height: 40,
  },
  submitButtonText: {
    color: 'white'
  }
});

class CameraPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      hasCameraPermission: null,
      type: Camera.Constants.Type.back,
      pictureSize: undefined,
      pictureSizes: [],
      pictureSizeId: 0,
      zoom: 0,
      preview: false,
      clickedImage: {},
      dataToSend: {
        name: null,
        phoneNumber: null,
        message: null,
        location: null,
        file: null
      }
    }
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentDidMount() {
    this._requestCameraPermission();
  }

  componentWillUnmount() {
    console.log('unmount', this.camera);
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);

  }
  _requestCameraPermission = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({
      hasCameraPermission: status === 'granted',
    });
  };


  handleBackButtonClick = () => {
    console.log(this.props.navigation);
    this.props.navigation.popToTop();
    return true;
  }

  takePicture = () => {
    if (this.camera) {
      this.camera.takePictureAsync({ onPictureSaved: this.onPictureSaved });
    }
  };

  onPictureSaved = async (photo) => {
    this.setState({
      preview: true,
      clickedImage: photo,
      dataToSend: {
        ...this.state.dataToSend,
        file: photo
      }
    })
    const asset = await MediaLibrary.createAssetAsync(photo.uri);
    // this.props.pictureTaken(photo.uri);
  }

  collectPictureSizes = async () => {

    if (this.camera) {
      const pictureSizes = await this.camera.getAvailablePictureSizesAsync('4:3');
      let pictureSizeId = 0;
      if (Platform.OS === 'ios') {
        pictureSizeId = pictureSizes.indexOf('High');
      } else {
        // returned array is sorted in ascending order - default size is the largest one
        pictureSizeId = pictureSizes.length - 1;
      }
      this.setState({ pictureSizes, pictureSizeId, pictureSize: pictureSizes[pictureSizeId] });
    }
  };

  zoomOut = () => this.setState({ zoom: this.state.zoom - 0.1 < 0 ? 0 : this.state.zoom - 0.1 });
  zoomIn = () => this.setState({ zoom: this.state.zoom + 0.1 > 1 ? 1 : this.state.zoom + 0.1 });

  zoomButtons = () => {
    return (
      <View style={styles.zoomContainer}>
        <Ionicons name="ios-add" size={40} color='white' onPress={this.zoomOut} style={{ padding: 6 }} />
        <Ionicons name="ios-add" size={40} color='white' onPress={this.zoomIn} style={{ padding: 6 }} />
      </View>
    );
  }

  previousPictureSize = () => this.changePictureSize(1);
  nextPictureSize = () => this.changePictureSize(-1);

  changePictureSize = direction => {
    let newId = this.state.pictureSizeId + direction;
    const length = this.state.pictureSizes.length;
    if (newId >= length) {
      newId = 0;
    } else if (newId < 0) {
      newId = length - 1;
    }
    this.setState({ pictureSize: this.state.pictureSizes[newId], pictureSizeId: newId });
  };

  pictureSizePicker() {
    return (
      <View style={styles.pictureSizeChooser}>
        <Ionicons name="md-return-left" size={40} color='white' onPress={this.previousPictureSize} style={{ padding: 6 }} />
        <View style={styles.pictureSizeLabel}>
          <Text style={styles.textStyle}>{this.state.pictureSize}</Text>
        </View>
        <Ionicons name="md-return-right" size={40} color='white' onPress={this.nextPictureSize} style={{ padding: 6 }} />
      </View>);
  }

  cameraModeButton() {
    return (<Ionicons name="ios-reverse-camera" type='ionicon' size={50} color='white' onPress={() => {
      this.setState({
        type: this.state.type === Camera.Constants.Type.front
          ? Camera.Constants.Type.front
          : Camera.Constants.Type.back,
      });
    }} style={{ flex: 1, justifyContent: 'center' }} />);
  }

  handleName = (text) => {
    this.setState({
      dataToSend: {
        ...this.state.dataToSend,
        name: text
      }
    })
  }

  handlePhoneNumber = (text) => {
    this.setState({
      dataToSend: {
        ...this.state.dataToSend,
        phoneNumber: text
      }
    })
  }

  handleMessage = (text) => {
    this.setState({
      dataToSend: {
        ...this.state.dataToSend,
        message: text
      }
    })
  }

  // _getLocationAsync = async () => {
  //   let { status } = await Permissions.askAsync(Permissions.LOCATION);
  //   if (status !== 'granted') {
  //     alert('Permission to access location was denied');
  //   }
  //   let location = await Location.getCurrentPositionAsync({});
  //  };

  sendData = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      alert('Permission to access location was denied');
    }
    let location = await Location.getCurrentPositionAsync({});
    let dataToSend = this.state.dataToSend;
    dataToSend.location = location;
    console.log(dataToSend);
  }

  render() {
    const { hasCameraPermission } = this.state;

    if (hasCameraPermission === null) {
      return <View />;
    } else if (hasCameraPermission === false) {
      return <Text>No access to camera</Text>;
    } else if (!this.state.preview) {
      return (
        <Camera
          ref={ref => { this.camera = ref }}
          style={styles.camera}
          onCameraReady={this.collectPictureSizes}
          type={this.state.type}
          flashMode={Camera.Constants.FlashMode.auto}
          autoFocus={Camera.Constants.AutoFocus.on}
          zoom={this.state.zoom}
          whiteBalance={Camera.Constants.WhiteBalance.auto}
          pictureSize={this.state.pictureSize}>
          <View style={styles.topBar}>
            {this.pictureSizePicker()}
            <Ionicons
              name='md-close'
              color='white'
              size={30}
              onPress={() => {
                this.props.navigation.navigate('MainStack');
              }}
              style={{ position: 'absolute', top: 20, left: 10 }} />
          </View>
          <View style={styles.bottomBar}>
            {this.zoomButtons()}
            <Ionicons name='md-radio-button-off' color='white' size={70} onPress={this.takePicture} style={{ flex: 1, padding: 6 }} />
            {/* {this.cameraModeButton()} */}
          </View>
        </Camera>
      );
    }
    else {
      return (
        <View style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'center' }}>
          <ScrollView>
            <Image
              source={{ uri: this.state.clickedImage.uri }}
              style={{ width: width, height: height - 250, resizeMode: 'cover' }}
            />

            <TextInput style={styles.input}
              underlineColorAndroid="transparent"
              placeholder="Name"
              placeholderTextColor="#9a73ef"
              autoCapitalize="none"
              onChangeText={this.handleName} />

            <TextInput style={styles.input}
              underlineColorAndroid="transparent"
              placeholder="Phone Number"
              placeholderTextColor="#9a73ef"
              autoCapitalize="none"
              onChangeText={this.handlePhoneNumber} />

            <TextInput style={styles.input}
              underlineColorAndroid="transparent"
              placeholder="Message"
              placeholderTextColor="#9a73ef"
              autoCapitalize="none"
              onChangeText={this.handleMessage} />
            <View style={{ flex: 1, direction: 'column' }}>
              <TouchableOpacity
                style={styles.submitButton}
                onPress={this.sendData}
              >
                <Text style={styles.submitButtonText}> Submit </Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.submitButton}
                onPress={() => {
                  this.setState({
                    dataToSend: {},
                    preview: false
                  })
                }}
              >
                <Text style={styles.submitButtonText}> Cancel </Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </View>
      )
    }
  }
}

export default CameraPage;