import React, { Component } from 'react';
import { View, Text, Button, TouchableOpacity, Dimensions, Image, YellowBox, StyleSheet, Platform } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator } from 'react-navigation-drawer';
import MapView , { Marker } from 'react-native-maps';
import Constants from 'expo-constants';
import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions';
import { Ionicons } from '@expo/vector-icons';
import Side_Menu from './SideMenu';
import HamburgerIcon from './HamburgerIcon';
import List from './List';
import CameraScreen from './CameraScreen';
// import HttpExample from './http_example';
// import ImagesExample from './image_example';
import Inputs from './inputs';
import MapTheme from './MapTheme';
const {height,width} = Dimensions.get('window');

class Home extends Component {
  constructor(props) {
    super(props);
    YellowBox.ignoreWarnings([
      'Warning: componentWillMount is deprecated',
      'Warning: componentWillReceiveProps is deprecated',
    ]);

    this.state = {
      mapRegion: null,
      hasLocationPermissions: false,
      locationResult: null,
      marker: {
        latitude:17.4796031,
        longitude:78.3728449
      }
    };
  }

  componentDidMount() {
    this._getLocationAsync();
  }

  _handleMapRegionChange = mapRegion => {
    this.setState({ mapRegion });
  };

  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({
        locationResult: 'Permission to access location was denied',
      });
    } else {
      this.setState({ hasLocationPermissions: true });
    }
 
    let location = await Location.getCurrentPositionAsync({});
    // location = JSON.stringify(location)
    this.setState({ 
      locationResult: JSON.stringify(location),
    });
    
    // Center the map on the location we just fetched.
     this.setState({
       mapRegion: { 
        latitude: location.coords.latitude,
        longitude: location.coords.longitude,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421
      },
      marker: {
        latitude: location.coords.latitude,
        longitude: location.coords.longitude
      }
    });
   };

   openCamera = () => {
     alert('openCamera');
   }
  render() {
    return (
      <View style={styles.MainContainer}>
        <Text style={{ fontSize: 23 }}> Home Screen </Text>
        {
          this.state.locationResult === null ?
          <Text>Finding your current location...</Text> :
          this.state.hasLocationPermissions === false ?
            <Text>Location permissions are not granted.</Text> :
            this.state.mapRegion === null ?
            <Text>Map region doesn't exist.</Text> :
            <MapView
              style={{ alignSelf: 'stretch', height: height }}
              region={this.state.mapRegion}
              onRegionChange={this._handleMapRegionChange}
              showUserLocation={true}
              customMapStyle={MapTheme}
            >
              <Marker
                coordinate={this.state.marker}
                title='Your are here'
              />
            </MapView>
        }
        <View style={styles.cameraButtonView}>
          <TouchableOpacity style={styles.buttonStyle}
          onPress={() => this.props.navigation.navigate('CameraScreen')}
          >
            <Ionicons name="md-camera" size={40} color="white" />
        </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const FirstActivity_StackNavigator = createStackNavigator({
  First: {
    screen: Home,
    navigationOptions: ({ navigation }) => ({
      title: 'Home',
      headerLeft: <HamburgerIcon navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#FF9800'
      },
      headerTintColor: '#fff',
    })
  },
});

const SecondActivity_StackNavigator = createStackNavigator({
  Second: {
    screen: List,
    navigationOptions: ({ navigation }) => ({
      title: 'List',
      headerLeft: <HamburgerIcon navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#FF9800'
      },
      headerTintColor: '#fff',
    })
  },
});

const ThirdActivity_StackNavigator = createStackNavigator({
  Third: {
    screen: Inputs,
    navigationOptions: ({ navigation }) => ({
      title: 'Inputs',
      headerLeft: <HamburgerIcon navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#FF9800'
      },
      headerTintColor: '#fff',
    })
  },
});

const CameraScreen_StackNavigator = createStackNavigator({
  CameraScreen: {
    screen: CameraScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Camera',
      headerLeft: <HamburgerIcon navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#FF9800'
      },
      headerTintColor: '#fff',
    })
  },
});

const MyDrawerNavigator = createDrawerNavigator({
  MainStack: {
    screen: FirstActivity_StackNavigator
  },
  SecondStack: {
    screen: SecondActivity_StackNavigator
  },
  ThirdStack: {
    screen: ThirdActivity_StackNavigator
  },
  FourthStack:{
    screen: CameraScreen_StackNavigator
  }
},
  {
    contentComponent: Side_Menu,
    drawerWidth: Dimensions.get('window').width - 130,
  });

const App = createAppContainer(MyDrawerNavigator);
export default App;


const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
    paddingTop: (Platform.OS) === 'ios' ? 20 : 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  cameraButtonView:{
    position:'absolute',
    right:40,
    bottom: 80,
    height:70,
    width:70,
    backgroundColor:'#FF9800',
    borderRadius: 35,
    alignItems: 'center',
    justifyContent:'center',
  }
});